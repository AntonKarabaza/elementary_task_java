/**
 * First task package.
 */
package com.softserve.edu.task1;

import java.io.IOException;


/**
 * @author Karabaza Anton
 *
 */
public class App {

	/**
	 * @param args[0] - height of the chessboard
	 *        args[1] - width of the chessboard
	 */
	public static void main(String[] args) throws IOException {
		int height;
		int width;
		boolean valid;

		Chessboard board1 = new Chessboard();
		valid = checkArgs(args);
		if (valid) {
			height = Integer.parseInt(args[0]);
			width = Integer.parseInt(args[1]);
			board1.setParameters(height, width);
			board1.printChessboard();
		}
	}
	/**
	 *
	 * @param args[] - command string input
	 * @return valid - are parameters valid or not
	 * @throws IOException
	 */
	public static boolean checkArgs(String[] args) {
		int height = 0;
		int width = 0;
		boolean valid = true;

		if (args.length == 0) {
			System.out.println("Usage: App [arg1 arg2]\n"
					+ "Where:\n\t arg1 - height of chessboard"
					+ "\n\t arg2 - width of chessboard");
			valid = false;
			return valid;
		} else if (args.length != 2) {
			System.out.println("Incorrect number of values."
					+ " Usage: App [arg1 arg2]");
			valid = false;
			return valid;
			}

		try {
			height = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of height. Height - "
					+ "positive integer value.");
			valid = false;
			return valid;
		}

		try {
			width = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of width. Width - "
					+ "positive integer value.");
			valid = false;
			return valid;
		}

		if (height <= 0) {
			System.out.println("Incorrect input of height."
					+ " Height - positive integer value.");
			valid = false;
			return valid;
		}
		if (width <= 0) {
			System.out.println("Incorrect input of width."
					+ " Width - positive integer value.");
			valid = false;
			return valid;
		}

		return valid;	
	}
}
