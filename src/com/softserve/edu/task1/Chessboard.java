/**
 * First task package.
 */
package com.softserve.edu.task1;

/**
 * @author Karabaza Anton
 *
 */
public class Chessboard {
	private int height;
	private int width;

	Chessboard() {
		height = 0;
		width = 0;
	}

	Chessboard(int h, int w) {
		height = h;
		width = w;
	}

	public void setParameters(int h, int w) {
		height = h;
		width = w;
	}

	public void printChessboard() {
		for (int i = 1; i <= height; i++) {
			for (int j = 1; j <= width; j++) {
				if ((i % 2 == 1) || (i == 1)) {          /* Checking the line. */
						if ((j % 2 == 1) || (j == 1)) {  /* Checking the row. */
							System.out.print("*");
						} else {
							System.out.print(" ");
						}
				} else {
					if (j % 2 == 0) {					 /* Checking the row. */
						System.out.print("*");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println("");
		}
	}
}
