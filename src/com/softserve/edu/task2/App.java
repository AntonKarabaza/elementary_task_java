/**
 * Second task package
 */
package com.softserve.edu.task2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
/**
 * @author Karabaza Anton
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {		
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));

		System.out.println("The program helps to check is it pissible \n"
				+ "to place one envelope inside of another envelope. \n"
				+ "To compare envelopes you have to enter 2 parameters\n"
				+ "(height and width) for each envelope.");
		
		Envelope env1 = new Envelope();
		Envelope env2 = new Envelope();
		String end = "y";
		String[] params = new String[2];
		boolean suitable = false;

		while (end.equalsIgnoreCase("y") || end.equalsIgnoreCase("yes")) {
			params = getParams();
			env1.setParameters(Double.parseDouble(params[0]),
					Double.parseDouble(params[1]));

			params = getParams();
			env2.setParameters(Double.parseDouble(params[0]),
					Double.parseDouble(params[1]));

			suitable = env1.compareEnvs(env2);
			if (suitable) {
				System.out.println("The one envelop can be placed"
						+ " inside of another envelope.");
			} else {
				System.out.println("The envelopes are not suitable"
						+ " to each other.");
			}
			System.out.println("Do you want to compare anothe pair"
					+ " of envelopes?(Y/N)");
			end = reader.readLine();
		}
	}
	/**
	 *
	 * @return params[] - parameters(height and width) for 1 envelope
	 * @throws IOException
	 */
	public static String[] getParams() throws IOException {
		String[] params = new String[2];
		double height = 0;
		double width = 0;

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));

		System.out.print("Enter height of the envelope: ");
		try {
			height = Double.parseDouble(reader.readLine());
			params[0] = Double.toString(height);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of height.");
			System.out.print("Height: ");
			params[0] = reader.readLine();
		}

		System.out.print("Enter width of the envelope: ");
		try {
			width = Double.parseDouble(reader.readLine());
			params[1] = Double.toString(width);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of width.");
			System.out.print("Width: ");
			params[1] = reader.readLine();
		}

		params = checkParams(params);

		return params;
	}
	/**
	 * 
	 * @param params[] - parameters to check
	 * @return params[] - valid parameters for envelope
	 * @throws IOException
	 */
	public static String[] checkParams(String[] params) throws IOException {
		double height = 0;
		double width = 0;
		boolean checked = true;

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));

		try {
			height = Double.parseDouble(params[0]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of height.");
			System.out.print("Height: ");
			params[0] = reader.readLine();
			checked = false;
		}

		try {
			width = Double.parseDouble(params[1]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of width.");
			System.out.print("Width: ");
			params[1] = reader.readLine();
			checked = false;
		}

		if (checked == false) {
			params = checkParams(params);
			height = Double.parseDouble(params[0]);
			width = Double.parseDouble(params[1]);
			checked = true;
		}

		if (height <= 0) {
			System.out.println("Incorrect input of height."
					+ " Height have to be more than 0");
			System.out.print("Height: ");
			params[0] = reader.readLine();
			checked = false;
		}
		if (width <= 0) {
			System.out.println("Incorrect input of width."
					+ " Width have to be more than 0");
			System.out.print("Width: ");
			params[1] = reader.readLine();
			checked = false;
		}

		if (checked == false) {
			params = checkParams(params);	
		} else {
			params[0] = Double.toString(height);
			params[1] = Double.toString(width);
		}
		return params;
	}

}
