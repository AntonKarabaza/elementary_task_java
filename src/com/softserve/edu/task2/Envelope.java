/**
 * Second task package
 */
package com.softserve.edu.task2;

/**
 * @author Karabaza Anton
 *
 */
public class Envelope {
	private double height;
	private double width;

	Envelope() {
		height = 0;
		width = 0;
	}

	Envelope(double h, double w) {
		height = h;
		width = w;
	}

	public void setParameters(double h, double w) {
		height = h;
		width = w;
	}

	public boolean compareEnvs(Envelope Env) {
		boolean suitable = false;

		if ((this.height < Env.height) && (this.width < Env.width)) {
			suitable = true;
		} else if ((this.height > Env.height) && (this.width > Env.width)) {
			suitable = true;
		} else if ((this.height < Env.width) && (this.width < Env.height)) {
			suitable = true;
		} else if ((this.height > Env.width) && (this.width > Env.height)) {
			suitable = true;
		}
		return suitable; 
	}
}
