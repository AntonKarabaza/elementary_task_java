/**
 * Third task package
 */
package com.softserve.edu.task3;

import java.util.Collections;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
/**
 * @author Karabaza Anton
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));

		System.out.println("This program helps to build a list of\n"
				+ "triangles in descending order by their area.\n"
				+ "You have to enter name of triangle and length\n"
				+ "of sides in format <name>,<val>,<val>,<val>.\n");

		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		String[] sepParams = new String[4];
		String params;
		String end = "y";
		double a;
		double b;
		double c;
		
		while (end.equalsIgnoreCase("y") || end.equalsIgnoreCase("yes")) {
			System.out.print("Enter parameters of triangle: ");
			params = reader.readLine();
			sepParams = params.replaceAll(" ", "").split(",");
			if (sepParams.length != 4) {
				System.out.println("Incorrect number of parameters."
						+ " The triangle wasn't \nadded."
						+ " Enter format <name>,<val>,<val>,<val>.");
			} else {
				try {
					a = Double.parseDouble(sepParams[1]);
					b = Double.parseDouble(sepParams[2]);
					c = Double.parseDouble(sepParams[3]);
					if (((a + b) > c) && ((a + c) > b) && ((b + c) > a)) {
						triangles.add(new Triangle(sepParams));
					} else {
						System.out.println("The sides with such length can't "
								+ "form triangle.");
					}
				 } catch (NumberFormatException e){
					 System.out.println("The triangle wasn't added. Input "
					 		+ "correct values.");
				 }
			}
			System.out.println("Do you want to add another one?(Y/N)");
			end = reader.readLine();
		}

		Collections.sort(triangles);
		System.out.println("========== Triangles list: ==========");
		for (Triangle tr: triangles) {
			tr.printTriangle();
		}
	}

}
