/**
 * Third task package
 */
package com.softserve.edu.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
/**
 * @author Karabaza Anton
 *
 */
public class Triangle implements Comparable<Triangle> {
	private String name;
	private double a;
	private double b;
	private double c;

	Triangle() {
		name = "";
		a = 0;
		b = 0;
		c = 0;
	}

	Triangle(String trname, double s1, double s2, double s3) {
		name = trname;
		a = s1;
		b = s2;
		c = s3;
	}
	/**
	 * 
	 * @param sepParams[] - parameters to check and set to
	 * 						class fields
	 * @throws IOException
	 */
	Triangle(String[] sepParams) throws IOException {
		sepParams = checkParams(sepParams);
		name = sepParams[0];
		a = Double.parseDouble(sepParams[1]);
		b = Double.parseDouble(sepParams[2]);
		c = Double.parseDouble(sepParams[3]);

	}

	/**
	 * 
	 * @param params[] - input parameters
	 * @return params[] - valid parameters for triangle
	 * @throws IOException
	 */
	private String[] checkParams(String[] params) throws IOException  {
			double a = Double.parseDouble(params[1]);
			double b = Double.parseDouble(params[2]);
			double c = Double.parseDouble(params[3]);
			boolean checked = true;

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(System.in));

			if (a <= 0) {
				System.out.println("Incorrect input of side A length."
						+ " Lenght have to be more than 0");
				System.out.print("Length: ");
				params[1] = reader.readLine();
				checked = false;
			}
			if (b <= 0) {
				System.out.println("Incorrect input of side B length."
						+ " Length have to be more than 0");
				System.out.print("Length: ");
				params[2] = reader.readLine();
				checked = false;
			}
			if (c <= 0) {
				System.out.println("Incorrect input of side C length."
						+ " Length have to be more than 0");
				System.out.print("Length: ");
				params[3] = reader.readLine();
				checked = false;
			}

			if (checked == false) {
				params = checkParams(params);	
			} else {
				params[1] = Double.toString(a);
				params[2] = Double.toString(b);
				params[3] = Double.toString(c);
			}
			return params;
	}

	public double getArea() {
		double p;
		double area;

		p = (a + b + c) / 2;
		area = Math.sqrt(p * (p - a) * (p - b) * (p - c));

		return area;
	}

	public void printTriangle() {
		System.out.println("[Triangle " + name + "]: " + 
				new DecimalFormat("#0.00").format(getArea()) + " cm");
	}

	@Override
	/* First compare areas. If equal - compare names. */
 	public int compareTo(Triangle tr) {
		if (this.getArea() != tr.getArea()) {
			if (this.getArea() > tr.getArea()) {
				return  -1;
			} else {
				return 1;
			}
		} else {
			return this.name.compareTo(tr.name);
		}
	}
}
