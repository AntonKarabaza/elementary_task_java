/**
 * Fourth task package
 */
package com.softserve.edu.task4;

import java.io.IOException;

/**
 * @author Karabaza Anton
 *
 */
public class App {

	private static final int COUNT_MODE = 2;
	private static final int REPLACE_MODE = 3;
 /**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		chooseMode(args);

	}

	public static void chooseMode(String[] args) throws IOException {

		if ((args.length != COUNT_MODE) && (args.length != REPLACE_MODE)) {
			System.out.println("Usage: App [arg1 arg2 arg3]\n"
					+ "Where:\n\t arg1 - file path to work with"
					+ "\n\t arg2 - 1) string, which number of entries have "
					+ "to be counted (if arg3 is empty)"
					+ "\n\t\t2) string, which have to be replaced (if arg3"
					+ " is not empty)"
					+ "\n\t arg3 - string, which have to replace the arg2 "
					+ "string");
		} else if (args.length == COUNT_MODE) {
			int count;
			String filePath = args[0];

			FileParser filePars = new FileParser(filePath);
			count = filePars.countStrings(args[1]);
			System.out.println("File has " + count + " strings.");
		} else if (args.length == REPLACE_MODE) {
			String filePath = args[0];
			FileParser filePars = new FileParser(filePath);
			int count;

			count = filePars.replaceString(args[1], args[2]);
			if (count == 0) {
				System.out.println("The strings to replace were not found.");
			} else {
				System.out.println("The strings were successfully replaced.");
			}
		}
	}
}
