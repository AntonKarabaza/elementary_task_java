/**
 * Fourth task package
 */
package com.softserve.edu.task4;

import java.io.*;
import java.util.ArrayList;
/**
 * @author Karabaza Anton
 *
 */
public class FileParser {
	private static final String FILE_NOT_FOUND_EX = "File not found."; 

	private String filePath;

	FileParser(String fP) {
		filePath = fP;
	}
	/**
	 * 
	 * @param strToCount - string, which number of entries have to be counted
	 * @return count - count of string entries in file
	 * @throws IOException
	 */
	public int countStrings(String strToCount) throws IOException {
		int count = 0;
		String someString;

		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))){
			while ((someString = reader.readLine()) != null) {
				if (someString.equals(strToCount)) {
					count++;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(FILE_NOT_FOUND_EX);
			System.exit(-1);
		}

		return count;
	}
	/**
	 * 
	 * @param strToSearch -  string, which have to be replaced
	 * @param strToReplace - string, which have to replace strToSearch
	 * @return count - count of strings which were replaced
	 * @throws IOException
	 */
	public int replaceString(String strToSearch, String strToReplace) throws IOException {
		ArrayList<String> fileStrings = new ArrayList<String>();
		String testStr;
		int count = 0;

		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
			while ((testStr = reader.readLine()) != null) {
				if (testStr.equals(strToSearch)) {
					fileStrings.add(strToReplace);
					count++;
				} else {
					fileStrings.add(testStr);
				}
			}
		} catch (FileNotFoundException e){
			System.out.println(FILE_NOT_FOUND_EX);
			System.exit(-1);
		}

		if (count == 0) {
			return count;
		}

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, false))) {
			for (String st: fileStrings) {
				writer.write(st + "\r\n");
			}
		} catch (FileNotFoundException e){
			System.out.println(FILE_NOT_FOUND_EX);
			System.exit(-1);
		}

		return count;
	}
}