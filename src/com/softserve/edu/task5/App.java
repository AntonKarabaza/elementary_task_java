/**
 * Fifth task package
 */
package com.softserve.edu.task5;

import java.io.IOException;

/**
 * 
 * @author Karabaza Anton
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		int num;

		if (checkArg(args)) {
			try {
				num = Integer.parseInt(args[0]);
				Converter conv = new Converter(num);
				System.out.println(("You have entered: " + conv.convert() + "("
						+ conv.getNumber() + ").").replaceAll("\\s+", " "));
			} catch (NumberFormatException e) {
				System.out.println("The argument have to be integer value.");
			} catch (NumRangeException e) {
				System.out.println("The number to convert have to be integer"
						+ " value in range from -1_000_000_000 to " + "+1_000_000_000.");
			}
		}
	}

	public static boolean checkArg(String[] arg) throws IOException {
		boolean valid = true;

		if (arg.length == 0) {
			System.out.println("Usage: App [arg1]\n" + "Where:\n\t arg1 - a number is needed to be converted "
					+ "in capital format.");
			valid = false;
			return valid;
		} else if (arg.length != 1) {
			System.out.println("Incorrect number of arguments. Usage: App [arg1]");
			valid = false;
			return valid;
		}

		return valid;
	}

}