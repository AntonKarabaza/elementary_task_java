/**
 * Fifth task package
 */
package com.softserve.edu.task5;

import java.util.HashMap;

/**
 * @author Karabaza Anton
 *
 */
public class Converter {
	private int number;

	Converter(int number) throws NumRangeException {
		if (checkNumber(number)) {
			this.number = number;
		}
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) throws NumRangeException {
		if (checkNumber(number)) {
			this.number = number;
		}
	}

	/**
	 * 
	 * @return resultStr - capital format of number
	 */
	public String convert() {
		HashMap<Integer, String> dict = new Dictionary().getDict();
		String resultStr = "";
		int workNum = number;

		if (workNum < 0) {
			resultStr = "minus ";
			workNum *= -1;
		}
		if (workNum < 20) {
			resultStr += dict.get(workNum);
		} else if (workNum < 100) {
			resultStr += dict.get(workNum / 10 * 10);
			if (dict.get(workNum % 10) != "zero") {
				resultStr += " " + dict.get(workNum % 10);
			}
		} else if (workNum < 1_000) {
			resultStr += getTriad(workNum, dict);
		} else if (workNum < 1_000_000) {
			resultStr += getTriad(workNum / 1000, dict) + " thousand";
			resultStr += " " + getTriad(workNum % 1000, dict);
		} else if (workNum < 1_000_000_000) {
			resultStr += getTriad(workNum / 1000000, dict) + " million";
			if (getTriad(workNum % 1000000 / 1000, dict) != "") {
				resultStr += " " + getTriad(workNum % 1000000 / 1000, dict) + " thousand";
			}
			resultStr += " " + getTriad(workNum % 1000, dict);
		}

		return resultStr;
	}

	/**
	 * 
	 * @param triad - triad of numbers from source number to convert in string
	 * @param dict - dictionary to convert digits in capital format
	 * @return resultStr - triad converted in capital format
	 */
	public String getTriad(int triad, HashMap<Integer, String> dict) {
		String resultStr = "";

		if (dict.get(triad / 100) != "zero") {
			resultStr = dict.get(triad / 100) + " hundred";
		}
		if (((triad % 100) < 20) && (dict.get(triad % 100) != "zero")) {
			resultStr += " " + dict.get(triad % 100);
		} else if (dict.get(triad % 100 / 10 * 10) != "zero") {
			resultStr += " " + dict.get(triad % 100 / 10 * 10);
			if (dict.get(triad % 10) != "zero") {
				resultStr += " " + dict.get(triad % 10);
			}
		}
		return resultStr;
	}

	/**
	 * 
	 * @param num - number to check
	 * @return valid - if the number falls in necessary range
	 * @throws NumRangeException - exception of exceeding the range
	 */
	private boolean checkNumber(int num) throws NumRangeException {
		boolean valid = true;

		if ((num <= -1000000000) || num >= 1000000000) {
			throw new NumRangeException();
		}
		return valid;
	}
}
