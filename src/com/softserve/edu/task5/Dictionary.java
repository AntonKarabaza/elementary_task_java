/**
 * Fifth task package
 */
package com.softserve.edu.task5;

import java.util.HashMap;

/**
 * @author Karabaza Anton
 *
 */
public class Dictionary {
	HashMap<Integer, String> dictionary = new HashMap<Integer, String>();
	
	Dictionary() {
		dictionary.put(0, "zero");
		dictionary.put(1, "one");
		dictionary.put(2, "two");
		dictionary.put(3, "three");
		dictionary.put(4, "four");
		dictionary.put(5, "five");
		dictionary.put(6, "six");
		dictionary.put(7, "seven");
		dictionary.put(8, "eight");
		dictionary.put(9, "nine");
		dictionary.put(10, "ten");
		dictionary.put(11, "eleven");
		dictionary.put(12, "twelve");
		dictionary.put(13, "thirteen");
		dictionary.put(14, "fourteen");
		dictionary.put(15, "fifteen");
		dictionary.put(16, "sixteen");
		dictionary.put(17, "seventeen");
		dictionary.put(18, "eighteen");
		dictionary.put(19, "nineteen");
		dictionary.put(20, "twenty");
		dictionary.put(30, "thirty");
		dictionary.put(40, "forty");
		dictionary.put(50, "fifty");
		dictionary.put(60, "sixty");
		dictionary.put(70, "seventy");
		dictionary.put(80, "eighty");
		dictionary.put(90, "ninety");
	}
	
	public HashMap<Integer, String> getDict() {
		return dictionary;
	}
}
