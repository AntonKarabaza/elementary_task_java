/**
 * Fifth task package
 */
package com.softserve.edu.task5;

/**
 * The class of exception which is used if
 * allowed range was exceeded
 * 
 * @author Karabaza Anton
 */
public class NumRangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
