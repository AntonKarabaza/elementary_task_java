/**
 * Sixth task package
 */
package com.softserve.edu.task6;

import java.io.*;

/**
 * @author Karabaza Anton
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		String mode = "";
		int count;

		checkArg(args);
		try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
			mode = reader.readLine().toLowerCase();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
			System.exit(-1);
		}

		switch (mode) {
		case "moscow":
			count = new Tickets().countMoscow();
			System.out.println("The count of happy tickets by Moscow method is: "
					+ count);
			break;
		case "petersburg":
			count = new Tickets().countPetersburg();
			System.out.println("The count of happy tickets by Petersburg method is: "
					+ count);
			break;
		default:
			System.out.println("Wrong input in file. The methods iput: "
					+ "\"Moscow\" or \"Petersburg\".");
		}
	}

	public static void checkArg(String[] arg) throws IOException {

		if (arg.length == 0) {
			System.out.println("The programm helps to count the number of happy\n"
					+ "tickets using two methods: Moscow and Petersburg. The\n"
					+ "config file must have one keyword \"Moscow\" or \"Petersburg\"\n"
					+ "to count tickets an appropriate method.\n\n"
					+ "Usage: App [arg1]\n"
					+ "Where:\n\t arg1 - a path to config file");
			System.exit(0);
		} else if (arg.length != 1) {
			System.out.println("Incorrect number of arguments. Usage: App [arg1]");
			System.exit(-1);
		}

	}
}
