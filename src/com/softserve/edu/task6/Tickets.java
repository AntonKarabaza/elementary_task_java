/**
 * Sixth task package
 */
package com.softserve.edu.task6;

/**
 * @author Karabaza Anton
 *
 */
public class Tickets {
	public static final int END_OF_COUNT = 54;

	private int ticket[] = { 0, 0, 0, 0, 0, 1 };

	Tickets() {}

	public int countMoscow() {
		int happyCount = 0;
		
		while (sum() <= END_OF_COUNT) {
			if (ticket[0] + ticket[1] + ticket[2] == ticket[3] + ticket[4] + ticket[5]) {
				happyCount++;
			}
			if (sum() == END_OF_COUNT){
				break;
			}
			getNext();
		}
		return happyCount;
	}
	
	public int countPetersburg() {
		int happyCount = 0;
		
		while (sum() <= END_OF_COUNT) {
			if (ticket[0] + ticket[2] + ticket[4] == ticket[1] + ticket[3] + ticket[5]) {
				happyCount++;
			}
			if (sum() == END_OF_COUNT){
				break;
			}
			getNext();
		}
		return happyCount;
	}

	private int sum() {
		int sum = 0;

		for (int i = 0; i < 6; i++) {
			sum += ticket[i];
		}
		return sum;
	}

	private void getNext(){
		
		for (int i = 5; i < 6; i--) {
			if(ticket[i] >= 9){
				ticket[i] = 0;
				ticket[i-1]++;
				if(ticket[i-1] < 10){
					break;
				}
			} else {
				ticket[i]++;
				break;
			}
		}
	}
}
