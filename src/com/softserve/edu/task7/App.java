/**
 * Seventh task package
 */
package com.softserve.edu.task7;

import java.io.IOException;
/**
 * @author Karabaza Anton
 *
 */
public class App {
	public static final String INPUT_ERR = "Incorrect input of argument."
			+ " Argument - positive integer value.";
	/**
	 * @param args[] - boundary value for series
	 */
	public static void main(String[] args) throws IOException {
		int number;
		boolean valid;

		valid = checkArg(args);
		if (valid) {
			number = Integer.parseInt(args[0]);
			System.out.println("A series of natural numbers is: ");
			for (int i = 1; number > Math.pow(i, 2); i++) {
				System.out.print(i);
				if (number > Math.pow((i + 1), 2)) {
					System.out.print(", ");
					if ((i % 20) == 0) {
						System.out.print("\n");
					}
				} else {
					System.out.print(".");
				}
			}
		}
	}
	/**
	 * 
	 * @param arg - argument to check
	 * @return - is argument valid or not
	 * @throws IOException
	 */

	public static boolean checkArg(String[] arg) throws IOException {
		int argument = 0;
		boolean valid = true;

		if (arg.length == 0) {
			System.out.println("Usage: App [arg1]\n"
					+ "Where:\n\t arg1 - a boundary value for series.");
			valid = false;
			return valid;
		} else if (arg.length != 1) {
			System.out.println("Incorrect number of arguments. Usage: App [arg1]");
			valid = false;
			return valid;
			}

		try {
			argument = Integer.parseInt(arg[0]);
		} catch (NumberFormatException e) {
			System.out.println(INPUT_ERR);
			valid = false;
			return valid;
		}

		if (argument <= 0) {
			System.out.println(INPUT_ERR);
			valid = false;
			return valid;
		}

		return valid;
	}
}