/**
 * Eights task package
 */
package com.softserve.edu.task8;

import java.io.IOException;
import java.util.ArrayList;
/**
 * @author Karabaza Anton
 *
 */
public class App {
	/**
	 * @param args[] - boundary values of the range
	 */
	public static void main(String[] args) throws IOException {
		boolean valid;

		valid = checkArgs(args);
		
		if (valid) {
			Fibonacci fibSeq = new Fibonacci();
			ArrayList<Integer> numbers = new ArrayList<Integer>();
			int rangeSt = Integer.parseInt(args[0]);
			int rangeEnd = Integer.parseInt(args[1]);
			
			numbers = fibSeq.getNumbers(rangeSt, rangeEnd);

			System.out.print("The sequence in range is: ");
			
			for(int i = 0; i < numbers.size(); i++){
				System.out.print(numbers.get(i));
				if (i < (numbers.size() - 1)) {
					System.out.print(", ");
				} else {
					System.out.print(".");
				}
			}
		}
	}
	
	/**
	 * 
	 * @param args[] - boundary values to check
	 * @return valid - are parameters valid or not
	 * @throws IOException
	 */
	public static boolean checkArgs(String[] args) throws IOException {
		int rangeSt = 0;
		int rangeEnd = 0;
		boolean valid = true;

		if (args.length == 0) {
			System.out.println("The program helps to get the list of"
					+ " Fibonacci numbers, which fall in the asked range.\n\n"
					+ "Usage: App [arg1 arg2]\n"
					+ "Where:\n\t arg1 - start of range"
					+ "\n\t arg2 - end of range");
			valid = false;
			return valid;
		} else if (args.length != 2) {
			System.out.println("Incorrect input of range."
					+ " Usage: App [arg1 arg2]");
			valid = false;
			return valid;
			}

		try {
			rangeSt = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of range start. Range start"
					+ " - integer value.");
			valid = false;
			return valid;
		}

		try {
			rangeEnd = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Incorrect input of range end. Range end"
					+ " - integer value.");
			valid = false;
			return valid;
		}
		
		if (rangeSt > rangeEnd) {
			System.out.println("Incorrect input of range star and end.");
			valid = false;
			return valid;
		}
		return valid;
	}
}
