/**
 * Eights task package
 */
package com.softserve.edu.task8;

import java.util.ArrayList;
/**
 * @author Karabaza Anton
 *
 */
public class Fibonacci {

	private final int f0 = 0;
	private final int f1 = 1;
	private int counter = 0;
	private ArrayList<Integer> f = new ArrayList<Integer>();

	Fibonacci() {
		f.add(f0);
		f.add(f1);
		counter = 2;
	}
	/**
	 * 
	 * @param rangeSt - start range value
	 * @param rangeEnd - end range value
	 * @return numbers - list of Fibonacci numbers, which fall in the
	 * 					asked range
	 */
	public ArrayList<Integer> getNumbers(int rangeSt, int rangeEnd) {
		ArrayList<Integer> numbers = new ArrayList<Integer>();

		do {
			f.add(f.get(counter - 1) + f.get(counter - 2));
			if (f.get(counter - 1) >= rangeSt && f.get(counter - 1) <= rangeEnd) {
				numbers.add(f.get(counter - 1));
			}
			counter++;
		} while (f.get(counter - 1) <= rangeEnd);

		if (numbers.size() != 0 && numbers.get(0) == 1) {
			numbers.add(0, f0);
		}

		return numbers;
	}
}
